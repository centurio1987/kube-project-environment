@ IN SOA coredns-coredns.dns.cluster.local. kimyoondeok.keyonbit.com. (
    123345  ;Serial
    21600   ;Refresh
    1800    ;Retry
    1209600 ;Expire
    86400   ;Minimum
)

@                               IN A 192.168.0.50
*.internal.keyonbit.com.        IN A 192.168.0.50
ns.internal.keyonbit.com        IN A 192.168.0.51