for i in $(kubectl get ns | awk '{print $1}');do
	kubectl get pod -n $i | awk '/Evicted/ {print $1}' | xargs kubectl delete pod -n $i 2> /dev/null
done
