resource "helm_release" "mysql" {
    name = "mysql"
    chart = "stable/mysql"

    values = [ "${file("terraform-provisioning/helm-values/mysql-values.yaml")}" ]
}

