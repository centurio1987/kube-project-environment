resource "helm_release" "nfs-client" {
    name = "nfs-client"
    chart = "stable/nfs-client-provisioner"

    set {
        name = "nfs.server"
        value = "192.168.0.21"
    }

    set {
        name = "nfs.path"
        value = "/volume1/ForCloud"
    }
  
}
