resource "helm_release" "opa-gatekeeper-operator" {
    name = "opa-gatekeeper-operator"
    chart = "./helm-chart/gatekeeper-operator"
  
    values = [ "${file("terraform-provisioning/helm-values/opa-gatekeeper-operator-values.yaml")}"]
}
