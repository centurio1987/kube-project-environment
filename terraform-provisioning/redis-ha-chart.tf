resource "helm_release" "redis-ha" {
    name = "redis-ha"
    chart = "stable/redis-ha"

    values = [ "${file("terraform-provisioning/helm-values/redis-ha-values.yaml")}"]
  
}
