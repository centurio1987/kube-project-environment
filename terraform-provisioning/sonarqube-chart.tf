resource "helm_release" "sonarqube" {
  name = "sonarqube"
  chart = "./helm-chart/sonarqube"
  namespace = "ci"

  values = [ "${file("terraform-provisioning/helm-values/sonarqube-values.yaml")}"]
}