resource "helm_release" "stolon" {
    name = "stolon"
    chart = "./helm-chart/stolon"

    values = [ "${file("terraform-provisioning/helm-values/stolon-values.yaml")}" ]

}
